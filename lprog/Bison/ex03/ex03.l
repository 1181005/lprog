%{
    #include <stdlib.h>
    #include "ex03.h"
    #include "ex03.tab.h"
    extern int numErros ; /* variável criada no bison */
%}

%%
\n				return yytext[0];
[-+]?[0-9]+		{yylval.val.valor=atoi(yytext);
                yylval.val.tipo=INT; 
                return TK_INT;}
[a-zA-Z]        {yylval.val.valor=yytext[0]; 
                yylval.val.tipo=CHAR;
                return TK_CHAR;}
\=              {return TK_EQ;}
\<\>            {return TK_NEQ;}
\<              {return TK_LT;}
\>              {return TK_GT;}
\<\=            {return TK_LE;}
\>\=            {return TK_GE;}
[ \t\r]
.	{
		printf ( "Erro lexico : simbolo desconhecido %s \n", yytext ) ;
		numErros++;
	}
<<EOF>> return 0; 

%%