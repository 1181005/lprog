%{
	#include <stdio.h>
	int numErrors = 0;

	int yyerror(char *s);
%}


%token TK_HELLO TK_WORLD TK_WORD
%start phrase

%%
phrase	: 	/* vazio */
	/* | phrase TK_HELLO TK_WORLD {printf("Hello World!!!\n");} */
	| phrase TK_HELLO
	| other TK_WORLD
	| phrase TK_WORD
	;
other : /*vazio*/
		| phrase TK_HELLO {printf("Hello World!!!\n");}
		| other TK_WORLD
		| phrase TK_WORD

%%

int main()
{
	yyparse();
}


int yyerror(char *s)
{
	numErrors++;
	printf("erro sintatico/semantico : %s\n", s);
}


