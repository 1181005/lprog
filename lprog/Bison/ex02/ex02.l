%{
    #include <stdlib.h>
%}


%%
\n  return yytext[0];

-?[0-9]+ {yylval=atoi(yytext); return TK_INT;}
\=  return TK_EQ
\>\= return TK_GEQ
\<\= return TK_LEQ
\> return TK_GT
\< return TK_LT
\<\> return TK_NEQ
. {
		printf ( "Erro lexico : simbolo desconhecido %s \n", yytext ) ;
		numErros++;
	}
<<EOF>> return 0; 
%%

