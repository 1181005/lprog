%{
#include <stdio.h>
int yylex();
int yyerror(char *);
int numErros=0;

%} 

%token TK_INT TK_EQ TK_GEQ TK_LEQ TK_GT TK_LT TK_NEQ


%start inicio

%%

inicio: 
        |inicio expressao '\n'
        ;


expressao:
        |TK_INT TK_EQ TK_INT {puts($1==$3?"Verdadeiro":"Falso");}
        |   TK_INT TK_NEQ TK_INT {puts($1!=$3?"Verdadeiro":"Falso");}
        |   TK_INT TK_LT TK_INT {puts($1<$3?"Verdadeiro":"Falso");}
        |   TK_INT TK_GT TK_INT {puts($1>$3?"Verdadeiro":"Falso");}
        |   TK_INT TK_LE TK_INT {puts($1<=$3?"Verdadeiro":"Falso");}
        |   TK_INT TK_GE TK_INT {puts($1>=$3?"Verdadeiro":"Falso");}
            ;


%%



int main(){
    yyparse();
    if (numErros==0)
		printf("Frase válida \n") ;
	else
		printf ("Frase inválida \nNúmero de erros : %d\n" ,numErros) ;


	return 0;

}
int yyerror (char *s) {
	numErros++;
	printf("erro sintactico/semantico:%s\n",s) ;
}