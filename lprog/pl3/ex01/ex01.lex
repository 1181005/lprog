%{
	#include <stdio.h>
	int qtdCaracteres=0, nLinhas=0;
%}

CARACTERES [a-zA-Z]

%%
   /* Se a ação for descrita numa só linha as chavetas podem ser omitidas */

\n	nLinhas++;
{CARACTERES}+	{printf("d %s \n",yytext);qtdCaracteres++;}
.

%%
int main ()
{
	yylex();
	printf("#linhas=%d\n",nLinhas);
	printf("#caracteres=%d\n",qtdCaracteres);
}

