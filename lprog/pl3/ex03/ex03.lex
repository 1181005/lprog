%{
	#include <stdio.h>
%}
CARACTERES [A-Z][A-Z]
ALGARISMOS [0-9][0-9]

ATE92 {CARACTERES}-{ALGARISMOS}-{ALGARISMOS}
DE92A05 {ALGARISMOS}-{ALGARISMOS}-{CARACTERES}
DE05A20 {ALGARISMOS}-{CARACTERES}-{ALGARISMOS}
DE20 {CARACTERES}[ ]{ALGARISMOS}[ ]{CARACTERES}

MATRICULAS {ATE92}|{DE92A05}|{DE05A20}|{DE20}

%%
{MATRICULAS} printf("Matricula valida!");
.* printf("INVALIDO!");
%%
int main(){a
    printf("Introduza a sua matricula: ");
    yylex();
}