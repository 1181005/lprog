%{
	#include <stdio.h>
    int qtdAlgarismos=0;int qtdLetras=0;int nLinhas=0;int qtdEspacos=0;int qtdCaracteres=0;
%}

ALGARISMO [0-9]
LETRA [a-zA-Z]
%%
{ALGARISMO} qtdAlgarismos++;
{LETRA} qtdLetras++;
" "|\t qtdEspacos++;
\n nLinhas++;
. qtdCaracteres++;

%%
int main(){
    yylex();
    printf("quantidade algarismos: %d\n",qtdAlgarismos);
    printf("quantidade de letras: %d\n",qtdLetras);
    printf("quantidade de linhas: %d\n", nLinhas);
    printf("quantidade de espaćos: %d\n", qtdEspacos);
    printf("quantidade de outros caracteres: %d\n", qtdCaracteres);
}

