
%{
	#include <stdlib.h>
	#include "bison_ex.tab.h" /* header gerado pelo bison */
	extern int numErros ; /* variável criada no bison */
%}

%%
,|\n				return yytext[0];
[-+]?[0-9]+			{yylval.inteiro = atoi(yytext); printf("i->%s<-\n", yytext);  return INT;}
[-+]?[0-9]+\.[0-9]+		{yylval.real = atof(yytext); printf("r->%s<-\n", yytext);  return REAL;}
[_a-zA-Z][_a-zA-Z0-9]*		{yylval.id = yytext; printf("s->%s<-\n", yytext);  return ID;}
[ \t\r]	/* ignorado */
.	{
		printf ( "Erro lexico : simbolo desconhecido %s \n", yytext ) ;
		numErros++;
	}

<<EOF>>		return 0 ;

%%

