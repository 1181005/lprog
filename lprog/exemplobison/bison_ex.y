
%{
 #include <stdio.h>
 int yylex();
 int yyerror (char *);
 
 int numArgs=0, numErros=0;
%}


%union {
	char *id;
	int inteiro;
	float real;
}

%token <id> ID 
%token <inteiro> INT 
%token <real> REAL
%type <real> arg lista_args

%start inicio

%%

inicio:	/*vazio*/
	| inicio lista_args '\n' {printf("=====> Soma = %f\n", $2);}
	| inicio '\n'
	| inicio error '\n'
	;
	
lista_args: arg {$$ = $1;}
	| lista_args ',' arg {$$ = $1 + $3;}
	;
	
arg:	ID	{printf("TS : %s\n", $1); $$ = 0; numArgs++;}
	| INT   {printf("TI : %d\n", $1); $$ = $1; numArgs++;}
	| REAL  {printf("TR : %f\n", $1); $$ = $1; numArgs++;}
	;

%%

int main () {
	yyparse () ;

	if (numErros==0)
		printf("Frase válida \n") ;
	else
		printf ("Frase inválida \nNúmero de erros : %d\n" ,numErros) ;

	printf ("Número de argumentos é %d\n",numArgs) ;

	return 0;
}

int yyerror (char *s) {
	numErros++;
	printf("erro sintactico/semantico:%s\n",s) ;
}

