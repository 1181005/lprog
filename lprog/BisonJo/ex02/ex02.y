%{
#include <stdio.h>
int yylex();
int yyerror(char *);
int numErros=0;

%}

%token TK_INT
%token TK_EQ TK_NEQ TK_LT TK_GT TK_LE TK_GE

%start inicio

%%

inicio: /*vazio*/
        |   inicio expressao '\n'
        |   inicio '\n'
        |   inicio error '\n'
        ;

expressao:      TK_INT TK_EQ TK_INT {puts($1==$3?"Verdadeiro":"Falso");}
            |   TK_INT TK_NEQ TK_INT {puts($1!=$3?"Verdadeiro":"Falso");}
            |   TK_INT TK_LT TK_INT {puts($1<$3?"Verdadeiro":"Falso");}
            |   TK_INT TK_GT TK_INT {puts($1>$3?"Verdadeiro":"Falso");}
            |   TK_INT TK_LE TK_INT {puts($1<=$3?"Verdadeiro":"Falso");}
            |   TK_INT TK_GE TK_INT {puts($1>=$3?"Verdadeiro":"Falso");}
            ;

/*
expressao: TK_INT operador TK_INT
        ;
operador TK_EQ | TK_NEQ | TK_LT | TK_GT | TK_LE | TK_GE
*/

%%

int main(){
    yyparse();
    if (numErros==0)
		printf("Frase válida \n") ;
	else
		printf ("Frase inválida \nNúmero de erros : %d\n" ,numErros) ;


	return 0;

}
int yyerror (char *s) {
	numErros++;
	printf("erro sintactico/semantico:%s\n",s) ;
}