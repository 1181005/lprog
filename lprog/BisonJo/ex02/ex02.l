%{
    #include <stdlib.h>
    #include "ex02.tab.h"
    extern int numErros ; /* variável criada no bison */
%}

%%
\n				return yytext[0];
[-+]?[0-9]+		{yylval=atoi(yytext); return TK_INT;}
\=              {return TK_EQ;}
\<\>            {return TK_NEQ;}
\<              {return TK_LT;}
\>              {return TK_GT;}
\<\=            {return TK_LE;}
\>\=            {return TK_GE;}
[ \t\r]
.	{
		printf ( "Erro lexico : simbolo desconhecido %s \n", yytext ) ;
		numErros++;
	}
<<EOF>> return 0; 

%%