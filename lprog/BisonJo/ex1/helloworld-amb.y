%{
	#include <stdio.h>
	int numErrors = 0;

	int yyerror(char *s);
%}

%token TK_HELLO TK_WORD TK_WORLD

%start phrase
%%

phrase : 
	|phrase TK_WORD
	|phrase TK_HELLO
	|other TK_WORLD
;
other : 
	| phrase TK_HELLO {printf("HELLO WORLD!!!\n");}
	| phrase TK_WORD
	| other TK_WORLD

%%

int main()
{
	yyparse();
}


int yyerror(char *s)
{
	numErrors++;
	printf("erro sintatico/semantico : %s\n", s);
}


