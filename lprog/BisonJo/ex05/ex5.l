
%{
	#include <stdlib.h>
	#include "pl3-ex5-2dg.tab.h" /* header gerado pelo bison */
	
	extern int numErros ; /* variável criada no bison */
%}

%%

[0-9]+			{return TK_INT;}
[0-9]+\.[0-9]+		{return TK_REAL;}
[-+*/=\n()]			return yytext[0];
[a-zA-Z]			{return TK_ID;}

[ \t\r]	/* ignorado */
.	{
		printf ( "Erro lexico : simbolo desconhecido %s \n", yytext ) ;
		numErros++;
	}

<<EOF>>		return 0 ;

%%

