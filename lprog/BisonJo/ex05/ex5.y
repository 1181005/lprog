%{
	#include <stdio.h>
	int numErrors = 0;

	int yylex();
	int yyerror(char *s);
%}


%token TK_INT 
%token TK_ID
%token TK_REAL
%start expressoes

%%
	/*
	S → ID ‘=‘ E|E
	E → E ‘+‘ E|E ‘-‘ E|E ‘*‘ E|E ‘/‘ E| ‘-‘E| ‘ (‘ E ‘) ‘ | ID | INT |REAL
	*/
	
expressoes:  /* vazio */
	| expressoes expressao '\n'
	| expressoes '\n'
	;

expressao: TK_ID '=' expr
	| expr
	;
	
expr:     expr '+' expr
	| expr '-' expr
	| expr '*' expr
	| expr '/' expr
	| '(' expr ')'
	| '-' expr
	| valor
	;
	
valor:    TK_ID
	| TK_INT
	| TK_REAL
	;

%%

int main()
{
	yyparse();
}


int yyerror(char *s)
{
	numErrors++;
	printf("erro sintatico/semantico : %s\n", s);
}


