%{
#include <stdio.h>
#include "ex03.h"

int yylex();
int yyerror(char *);

void compara(TValor v1, int op, TValor v2);
int numErros=0;

%}

%union {
	TValor val
}

%token <val> TK_INT TK_CHAR
%token TK_EQ TK_NEQ TK_LT TK_GT TK_LE TK_GE

%type <val> valor expressao

%start inicio

%%

inicio: /*vazio*/
        |   inicio expressao '\n'
        |   inicio '\n'
        |   inicio error '\n'
        ;

expressao:  valor {$$=$1;}
        |   expressao TK_EQ valor  {compara($1, TK_EQ, $3); $$=$1;}   
        |   expressao TK_NEQ valor {compara($1, TK_NEQ, $3); $$=$1;}  
        |   expressao TK_LT valor  {compara($1, TK_LT, $3); $$=$1;}  
        |   expressao TK_GT valor  {compara($1, TK_GT, $3); $$=$1;}  
        |   expressao TK_LE valor  {compara($1, TK_LE, $3); $$=$1;}  
        |   expressao TK_GE valor  {compara($1, TK_GE, $3); $$=$1;}  
        ;
valor: TK_CHAR  {$$=$1;}
        | TK_INT {$$=$1;}
        ;

%%

int main(){
    yyparse();
    if (numErros==0)
		printf("Frase válida \n") ;
	else
		printf ("Frase inválida \nNúmero de erros : %d\n" ,numErros) ;


	return 0;

}
int yyerror (char *s) {
	numErros++;
	printf("erro sintactico/semantico:%s\n",s) ;
}

void compara(TValor v1, int op, TValor v2){
        if(v1.tipo!=v2.tipo){
                puts("Incompativel");
                return;
        }

        switch(op){
                case TK_EQ: puts(v1.valor==v2.valor?"Verdadeiro":"Falso");
                break;
                case TK_NEQ: puts(v1.valor!=v2.valor?"Verdadeiro":"Falso");
                break;
                case TK_LT: puts(v1.valor<v2.valor?"Verdadeiro":"Falso");
                break;
                case TK_GT: puts(v1.valor>v2.valor?"Verdadeiro":"Falso");
                break;
                case TK_LE: puts(v1.valor<=v2.valor?"Verdadeiro":"Falso");
                break;
                case TK_GE: puts(v1.valor>=v2.valor?"Verdadeiro":"Falso");
                }
}
