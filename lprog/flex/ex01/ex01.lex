%{
    #include <stdio.h>
    int qtdCaracteres = 0, nLinhas=0; 
%}

CARACTERES [a-zA-Z]

%%
{CARACTERES}+	{printf("d %s \n",yytext);qtdCaracteres++;}

\n {nLinhas++;}
%%

int main()
{
    yylex();
    printf("Linhas=%d\n", nLinhas);
    printf("caracteres=%d\n", qtdCaracteres);
}