%{
	#include <stdio.h>
    int numeroValidas = 0; int numeroInvalidas = 0;
%}
CARACTERES [A-Za-z][A-Za-z]
ALGARISMOS [0-9][0-9]

ATE92 {CARACTERES}-{ALGARISMOS}-{ALGARISMOS}
DE92A05 {ALGARISMOS}-{ALGARISMOS}-{CARACTERES}
DE05A20 {ALGARISMOS}-{CARACTERES}-{ALGARISMOS}
DE20 {CARACTERES}[ ]{ALGARISMOS}[ ]{CARACTERES}

MATRICULAS {ATE92}|{DE92A05}|{DE05A20}|{DE20}

%%
{MATRICULAS} {printf("Matricula valida!");numeroValidas++;}
.* {printf("INVALIDO!");numeroInvalidas++;}
%%
int main(){
    printf("Introduza a sua matricula: ");
    yylex();

    printf("Invalidas: %d\nValidas: %d\n", numeroInvalidas,numeroValidas);
}