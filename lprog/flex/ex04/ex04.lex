%{
	#include <stdio.h>
    int numeroAlgarismos = 0; int numeroLetras = 0; int nLinhas= 0; int nTabelacoes=0; int numeroInvalidas= 0;
%}
ALGARISMOS [0-9]
LETRAS [a-zA-Z]

%%
{ALGARISMOS} {numeroAlgarismos++;}
{LETRAS} {numeroLetras++;}
\n {nLinhas++;}
\t|" " {nTabelacoes++;}
.* {numeroInvalidas++;}
%%

int main(){
    yylex();

    printf("quantidade algarismos: %d\n",numeroAlgarismos);
    printf("quantidade de letras: %d\n",numeroLetras);
    printf("quantidade de linhas: %d\n", nLinhas);
    printf("quantidade de espaćos: %d\n", nTabelacoes);
    printf("quantidade de outros caracteres: %d\n", numeroInvalidas);
}