%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    int aulasQtd=0;
    int muscQtd=0;

    int yylex();
    int yyerror(char *s);
%}



%union {
   int num;
   char *str;
}

%token <str> TK_DATA
%token <num> TK_INT
%token TK_HORA TK_CODAULA TK_NOME TK_AULA TK_MUSC
%type <num> musc grupo aula aulas 
%type <str> cabecalho
%%

inicio: 
        | cabecalho '\n' aulas '\n' {printf("Análise para o dia %s\nAulas com mais de 90 minutos: %d\nSessoes de musculacao superior a 90 minutos: %d\n Total de registos: %d\n", $1,aulasQtd,muscQtd, $3);}
        | cabecalho '\n' {printf("Análise para o dia %s\nAulas com mais de 90 minutos: 0\nSessoes de musculacao superior a 90 minutos: 0\n Total de registos: 0\n", $1);}
        ;

cabecalho: TK_DATA TK_HORA TK_INT { if($3>100){yyerror("Codigo funcionario errado.");} $$ = $1; } 
        | TK_DATA TK_HORA {$$ = $1;}  
        ;

aulas: aulas '\n' aula {$$ = $1 + $3;}
    | aula {$$ = $1;}
    | aulas '\n' erros
    ;

erros: error {yyerror("Linha com erro!");}
    | erros error
    ;

aula: musc  {$$ = 1;}
    | grupo {$$ = 1;}
    ;

musc: TK_MUSC TK_HORA TK_INT TK_INT TK_NOME  {if($3<1500){
                                                if($4>90){muscQtd++;}
                                            }else{
                                                yyerror("Codigo do cliente invalido.");
                                            }}
    | TK_MUSC TK_HORA TK_INT TK_INT {if($3<1500){
                                                if($4>90){muscQtd++;}
                                            }else{
                                                yyerror("Codigo do cliente invalido.");
                                            }}
    ;

grupo: TK_AULA TK_CODAULA TK_HORA TK_INT TK_INT TK_NOME {if($4<1500){
                                                if($5>90){aulasQtd++;}
                                            }else{
                                                yyerror("Codigo do cliente invalido.");
                                            }}
    | TK_AULA TK_CODAULA TK_HORA TK_INT TK_INT {if($4<1500){
                                                if($5>90){aulasQtd++;}
                                            }else{
                                                yyerror("Codigo do cliente invalido.");
                                            }}
    ;

%%

int yyerror(char *s) 
{ 
   printf("Erro: %s\n",s);
   return 0;
} 


int main() 
{ 
   yyparse(); 
}