%{
	#include <stdio.h>	
	#include <stdlib.h>
	#include <string.h>
	#include "ex09.tab.h" 
%} 

%%
(([1-2][0-9])|(3[01])|(0[1-9]))\-((0[1-9])|(1[-2]))\-([0-9]{4})     yylval.str=strdup(yytext); return TK_DATA;
(([01][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])                  return TK_HORA;
[1-9][0-9]*                                                 yylval.num=atoi(yytext);   return TK_INT;
[A-Z]{3}                                                    return TK_CODAULA;
\"[^\"]+\"        			                                return TK_NOME;
AULA                                                        return TK_AULA;
MUSC                                                        return TK_MUSC;            
\n                                                          return yytext[0];
[ \r\t]                         
.                                               {printf("Erro Lexico : carácter invalido (%s)\n", yytext);}
<<EOF>>     return 0;
%%